<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::post('/register', 'AuthController@register');

Route::post('/login', 'AuthController@login');
Route::post('/logout', 'AuthController@logout');

Route::middleware('jwt.auth')->group(function() {
    Route::get('/topic', 'TopicController@list');
    Route::post('/topic', 'TopicController@save');
    Route::put('/topic/{id}', 'TopicController@update');
    Route::delete('/topic/{id}', 'TopicController@destroy');
    Route::put('/end-topic/{id}', 'TopicController@endTopic');
    Route::put('/active-topic/{id}', 'TopicController@activeTopic');
});
Route::get('/topic/{id}', 'TopicController@detail');
Route::get('/active-topic', 'TopicController@getActiveTopic');
Route::put('/answer/{id}', 'AnswerController@countAnswer');

