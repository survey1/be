<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use App\Topic;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    public function __construct()
    {
        //$this->middleware('jwt.auth')->except(['detail']);
    }

    public function list()
    {
        $topics = Topic::all();
        return $this->responseTemplate(true, $topics, "Success get list topics");
    }

    public function save(Request $request)
    {
        $topic = new Topic();
        $topic->name = $request->name;
        $topic->description = $request->description;
        $topic->video = $request->video;
        $topic->save();

        if($request->questions && count($request->questions)) {
            foreach ($request->questions as $question) {
                $qt = new Question();
                $qt->name = $question->name;
                $qt->topic_id = $topic->id;
                $qt->save();

                if($question->answers && count($question->answers)) {
                    foreach ($question->answers as $answer) {
                        $as = new Answer();
                        $as->name = $answer->name;
                        $as->question_id = $question->id;
                        $as->type = $answer->type;
                        $as->save();
                    }
                }
            }
        }

        return $this->responseTemplate(true, $topic, "Success save topic");
    }

    public function detail($id)
    {
        $topic = Topic::find($id);
        if($topic) {
            $questions = Question::where('topic_id', $topic->id)->get();
            foreach ($questions as $question) {
                $answers = Answer::where('question_id', $question->id)->orderBy('type', 'asc')->get();
                $question->answers = $answers;
            }
            $topic->questions = $questions;
            return $this->responseTemplate(true, $topic, "Success get detail topic");
        } else {
            return $this->responseTemplate(false, null, "Failed get detail topic");
        }
    }

    public function update(Request $request, $id)
    {
        $topic = Topic::find($id);
        if($topic) {
            $topic->name = $request->name;
            $topic->description = $request->description;
            $topic->video = $request->video;
            $topic->save();

            // first delete all question and answer of that topic
            $questionIds = Question::where('topic_id', $topic->id)->pluck('id');
            if ($questionIds) {
                $answerIds = Answer::whereIn('question_id', $questionIds)->pluck('id');
                if($answerIds) {
                    $this->delItem('Answer', $answerIds);
                    $this->delItem('Question', $questionIds);
                }
            }
            // second create all new question and answer
            if($request->questions && count($request->questions)) {
                foreach ($request->questions as $question) {
                    $qt = new Question();
                    $qt->name = $question['name'];
                    $qt->topic_id = $topic->id;
                    $qt->save();

                    if($question['answers'] && count($question['answers'])) {
                        foreach ($question['answers'] as $answer) {
                            $as = new Answer();
                            $as->name = $answer['name'];
                            $as->question_id = $qt->id;
                            $as->type = $answer['type'];
                            $as->save();
                        }
                    }
                }
            }

            return $this->responseTemplate(true, $topic, "Success update topic");
        } else {
            return $this->responseTemplate(false, null, "Failed update topic");
        }
    }

    public function destroy($id)
    {
        $topic = Topic::find($id);
        if($topic) {
            $topic->delete();

            return $this->responseTemplate(true, $topic, "Success delete topic");
        } else {
            return $this->responseTemplate(false, null, "Failed delete topic");
        }
    }

    public function endTopic($id)
    {
        $topic = Topic::find($id);
        if($topic) {
            $topic->end = 1;
            $topic->active = 0;
            $topic->save();
            return $this->responseTemplate(true, $topic, "Success end topic");
        }
        return $this->responseTemplate(false, null, "Error");
    }

    public function getActiveTopic()
    {
        //dd(123);
        $topic = Topic::where('active', 1)->where('end', 0)->first();
        if($topic) {
            $questions = Question::where('topic_id', $topic->id)->get();
            foreach ($questions as $question) {
                $answers = Answer::where('question_id', $question->id)->get();
                $question->answers = $answers;
            }
            $topic->questions = $questions;
            return $this->responseTemplate(true, $topic, "Success get detail topic");
        } else {
            return $this->responseTemplate(false, null, "No active topic");
        }
    }

    public function activeTopic($id)
    {
        $topic = Topic::find($id);
        if($topic) {
            $topic->active = 1;
            $topic->save();
            Topic::where('id', '<>', $topic->id)->update(['active' => 0]);
            return $this->responseTemplate(true, $topic, "Success active topic");
        }
        return $this->responseTemplate(false, null, "Error");
    }

    protected function delItem($type, $ids)
    {
        if($type == 'Question') {
            $it = Question::whereIn('id', $ids)->delete();
        }
        else if($type == 'Answer') {
            $it = Answer::whereIn('id', $ids)->delete();
        }
    }

    protected function responseTemplate($success, $data, $message)
    {
        return response()->json([
            'success' => $success,
            'data'=> $data,
            'message' => $message
        ]);
    }
}
