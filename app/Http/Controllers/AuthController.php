<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use JWTFactory;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $user = User::where('name', $request->name)->first();
        if($user) {
            return response()->json(['error' => 'User existed!'], 409);
        } else {
            $user = User::create([
                'name' => $request->name,
                'password' => $request->password,
                'role' => $request->role ? $request->role : 'user'
            ]);

            $token = auth()->login($user);

            return $this->respondWithToken($token, $request->role ? $request->role : 'user');
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['name', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token, auth()->user()->role);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    protected function respondWithToken($token, $role)
    {
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60,
            'role' => $role
        ]);
    }
}
