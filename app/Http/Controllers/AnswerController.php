<?php

namespace App\Http\Controllers;

use App\Answer;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    public function countAnswer(Request $request, $id)
    {
        //dd(123);
        $answer = Answer::find($id);
        if ($answer) {
            $answer->count = $answer->count + $request->number;
            $answer->save();

            return $this->responseTemplate(true, $answer, 'Success update answer');
        }
        return $this->responseTemplate(false, null, 'Error');
    }

    protected function responseTemplate($success, $data, $message)
    {
        return response()->json([
            'success' => $success,
            'data'=> $data,
            'message' => $message
        ]);
    }
}
